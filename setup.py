from setuptools import setup

setup(name='GiftZZZ',
      version='1.0',
      description='OpenShift App',
      author='Your Name',
      author_email='example@example.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Django>=1.6', 'Pillow', 'South==0.8.4', 'Unidecode==0.04.14',
          'amqp==1.4.3', 'anyjson==0.3.3', 'billiard==3.3.0.16', 'celery==3.1.9',
          'django-celery==3.1.9', 'django-facebook==5.3.1', 'kombu==3.0.12',
          'pycrypto==2.6', 'pytz==2013.9', 'requests==2.2.1', 'MySQL-python',
          'django-storages', 'boto', 'psycopg2', 'newrelic==2.16.0.12']
     )
