from json import dumps

from django.http import HttpResponse

def json_response(data):
    return HttpResponse(dumps(data), mimetype='application/json')
