from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import urlresolvers
from django.contrib import messages
from django.shortcuts import get_object_or_404
import datetime
import settings
import json

from .models import Question, Survey, Category, Answer

from hiyya_web.utils import json_response

def get_survey(request):
    data = {}
    product_id = request.GET['product_id']
    survey = get_object_or_404(Survey, product=product_id)
    data['product_id'] = product_id
    data['survey_id'] = survey.id
    #data['user_id'] = request.user.id
    data['questions'] = []
    for q in survey.questions():
         data['questions'].append({
            'id': q.id,
            'text': q.text,
            'question_type': q.question_type,
            'choices': map(lambda x: x.strip(), q.choices.split(','))
         })
    return json_response(data)

def answer(request):
    product_id = int(str(request.GET.get('product_id')))
    survey_id = int(str(request.GET.get('survey_id')))
    question_id = int(str(request.GET.get('question_id')))
    answer_text = int(str(request.GET.get('answer')))
    user = request.user
    answer = Answer(
        user = user,
        product_id = product_id,
        survey_id = survey_id,
        question_id = question_id,
        answer = answer_text,
    )
    answer.save()
    return json_response({'id': 56})
