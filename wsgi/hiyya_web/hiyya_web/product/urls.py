from django.conf.urls import patterns, include, url
urlpatterns = patterns('hiyya_web.product.views',
    url(r'^list/(?P<status>\w+?)/$', 'products_list'),
    url(r'^notify_me/(?P<product_id>\d+)/$', 'notify_me'),
    url(r'^gift/$', 'gift_list'),
    url(r'^gift/(?P<gift_id>\d+)/$', 'change_gift_status'),
    url(r'^gift/expired/(?P<gift_id>\d+)$', 'make_expired'),
    url(r'^gift-send/$', 'gift_send'),
    url(r'^gift-add-address/$', 'gift_add_address'),
    url(r'^gift-accept-survey/$', 'gift_accept_survey'),
    url(r'^details/(?P<product_id>\d+)', 'to_product_redirect')
    )
