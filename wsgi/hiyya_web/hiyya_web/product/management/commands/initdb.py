#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function, unicode_literals

import os

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

products_data = [{
    'name': "Lanvin Eclat D'Arpege",
    'description': """Аромат на всі часи та випадки життя. Парфуми Еклат де Арпаж  від Ланвін уособлюють
        чистоту, ласкавий травневий вітерець, в якому ледь відчутні пахощі весняних квітів, м’яке
        сяюче сонечко, що надає синьому небосхилу якнайлегші відтінки.  Відчуйте себе вільною,
        окутаною в аромат весняних квітів, які тільки що пробудились і ще всипані вранішньою росою.
        Інтернет магазин Жером радить ці парфуми усім: молоденьким дівчаткам, а також поважним пані.
        З парфумами Eclat D'Arpege Lanvin нічого зайвого не потрібно, ніяких соціальних ролей та
        ігор, ніяких масок та статусів, тільки щирість та чистота, просто побудьте собою. Бережіть
        почуття, спогади та жіночність. Ціни на парфуми Ланві на сайті інтернет магазину Жером
        актуальні.""",
    'inventory_cnt': 394,

    'brand': "Gerome",
    'store_url': "http://gerome.com.ua/",
    'image': 'svitlana.jpg'
},
    {
    'name': "Зубний гель на травах, Веледа/Weleda",
    'description': """Не містить синтетичних миючих речовий, ароматизаторів, барвників, консервантів, мінеральних олій і продуктів їх переробки. Склад: рослинний гліцерин, кремнієва кислота, витяжки з квітів ромашки, кореня ратанії, мірри, олії м'яти перцевої та кучерявої, фенхеля, альгінат (натурального походження), спирт, ескулін.""",
    'inventory_cnt': 995,
    'brand': "Daska",
    'store_url': "http://dska.com.ua/katalog/4771-zubni-pasty/zubnyy-gel-na-travakh-veleda-weleda-75-ml/",
    'image': 'gell.png',

},

    {
    'name': "Флеш пам'ять USB 2 Gb Traxdata YEGO",
    'description': """Ємність:
2Gb
Виробник:
Traxdata
Інтерфейс:
USB 2.0""",

    'inventory_cnt': 983,
    'brand': "Дефіс",
    'store_url': 'http://defis.lviv.ua/156-.2-gb/229.html',
    'image': "flash.png"
}]


def get_image_path(name):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), name)


def open_image(name):
    return name,  File(open(get_image_path(name)))


def create_product(product):

    from hiyya_web.product.models import Brand, Product, ProductImage
    brand = Brand(name=product.pop('brand'))
    brand.save()
    path_image = product.pop('image')
    new_product = Product(**product)
    new_product.brand = brand
    new_product.save()
    image = ProductImage(main=True)
    image.product = new_product
    image.image.save(*open_image(path_image))
    image.save()


class Command(BaseCommand):

    help = 'Generate data for local testing'

    def handle(self, *args, **options):
        for product in products_data:
            create_product(product)
        raise CommandError('Invalid options')
