import datetime

from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.conf import settings

from django_facebook.models import FacebookUser

from hiyya_web.hiyya_user.models import VKProfile


PRODUCT_NEW = 'new'
PRODUCT_UPCOMING = 'upcoming'
PRODUCT_EXPIRED = 'expired'
PRODUCT_ALL = 'all'

UserModel = get_user_model()

GIFT_STATUS_CHOICES = (
    (1, 'New'),
    (2, 'Accepted'),
    (3, 'Sent'),
    (4, 'Expired'),
    (5, 'Rejected'),
)


def product_status(product):
    if product.inventory_cnt > 0:
        return PRODUCT_NEW
    elif product.inventory_cnt == 0:
        return PRODUCT_EXPIRED
    else:
        return PRODUCT_UPCOMING


class ProductStatusManager(models.Manager):

    def filter_status(self, status=PRODUCT_ALL, **kw):
        query_set = self.filter(**kw)

        if status == PRODUCT_NEW:
            return query_set.filter(inventory_cnt__gt=0)
        elif status == PRODUCT_EXPIRED:
            return query_set.filter(inventory_cnt=0)
        elif status == PRODUCT_EXPIRED:
            return query_set.filter(inventory_cnt__lt=0)
        return query_set


class Brand(models.Model):
    name = models.CharField(max_length=300)
    website = models.URLField(blank=True, null=True)

    def __unicode__(self):
        return self.name


class Product(models.Model):
    objects = ProductStatusManager()

    name = models.CharField(max_length=300)
    description = models.TextField()

    store_url = models.URLField()

    inventory_cnt = models.IntegerField()
    featured = models.BooleanField(default=False)

    created = models.DateField(auto_now=True, default=datetime.date.today())
    archived = models.BooleanField(default=False)

    brand = models.ForeignKey(Brand, related_name='products', null=True)

    notified_users = models.ManyToManyField(UserModel,
        related_name='watching_products', blank=True)

    def __unicode__(self):
        return self.name

    @property
    def status(self):
        if self.inventory_cnt > 0:
            return PRODUCT_NEW
        elif self.inventory_cnt == 0:
            return PRODUCT_EXPIRED
        elif self.inventory_cnt < 0:
            return PRODUCT_UPCOMING


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images')

    image = models.ImageField(upload_to='products')
    main = models.BooleanField(default=False)
    featured = models.BooleanField(default=False)

    def __unicode__(self):
        return u'{}:{}({})'.format(self.product.name, self.id,
                'main' if self.main else 'not main')


class Gift(models.Model):

    sender = models.ForeignKey(UserModel, related_name='sent_gifts')
    receiver = models.ForeignKey(UserModel, related_name='received_gifts',
        null=True, blank=True, default=None)
    receiver_fb = models.ForeignKey(FacebookUser, null=True)
    receiver_vk = models.ForeignKey(VKProfile, null=True)

    product = models.ForeignKey(Product)
    created = models.DateTimeField(auto_now_add=True)
    expired = models.BooleanField(default=False)

    status = models.IntegerField(max_length=1,
        choices=GIFT_STATUS_CHOICES, default=1)

    message = models.CharField(max_length=255, default='')

    def answers(self):
        from hiyya_web.survey.models import GiftAnswer
        if self.pk:
            return GiftAnswer.objects.filter(gift=self.pk)
        else:
            return None

    def __unicode__(self):
        if settings.SOCIAL_NETWORK == 'vk':
            return u'{} -> {}: {}'.format(self.sender, self.receiver_vk.first_name,
                self.product)
        else:
            return u'{} -> {}: {}'.format(self.sender, self.receiver_fb.name,
                self.product)


class Address(models.Model):
    gift = models.OneToOneField(Gift)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    city_st = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=10)
    phone = models.CharField(max_length=30, default='+380')
    country = models.CharField(max_length=20)

    def __unicode__(self):
        return u'{} -> {}'.format(
            self.gift.id, self.city_st)


@receiver(pre_save, sender=Product)
def check_if_status_changed(sender, instance, **kwargs):
    MAIL_TMPL = """Giftzzz is proud to announce that {product_name} is now available! Click this link to check it out and send it to friends."""
    try:
        obj = Product.objects.get(pk=instance.pk)
    except Product.DoesNotExist:
        pass
    else:
        if (obj.status == PRODUCT_UPCOMING and instance.status == PRODUCT_NEW):
            for user in instance.notified_users.all():
                #send mail
                # remove user
                send_mail('[Giftzzz] Product available', MAIL_TMPL.format(product_name=instance.name),
                    settings.EMAIL_HOST_USER, [user.email], fail_silently=True)
                instance.notified_users.remove(user)
