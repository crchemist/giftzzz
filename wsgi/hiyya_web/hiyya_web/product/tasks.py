from celery.decorators import task
from celery.decorators import periodic_task
from celery.task.schedules import crontab

from datetime import datetime, timedelta
from django.utils import timezone


GIFT_NEW_STATUS = 1

@periodic_task(run_every=crontab(minute=10))
def check_gift_expires():
    from hiyya_web.product.models import Gift
    now = timezone.now()
    day_ago = now - timedelta(days=1)
    for g in Gift.objects.filter(status=GIFT_NEW_STATUS,
            created__lte=day_ago, expired=False):
        g.expired = True
        g.save()
        g.product.inventory_cnt += 1
        g.product.save()
