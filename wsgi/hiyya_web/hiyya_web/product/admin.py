from django.contrib import admin

from .models import Product, Gift, ProductImage, Brand, Address

admin.site.register(Brand)
admin.site.register(Product)
admin.site.register(ProductImage)
admin.site.register(Gift)
admin.site.register(Address)
