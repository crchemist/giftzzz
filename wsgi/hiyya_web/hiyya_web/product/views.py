from django.core import serializers
from django.conf import settings
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils import timezone

import json

from hiyya_web.utils import json_response
from django_facebook.models import FacebookUser
from hiyya_web.survey.models import Answer, Survey, GiftAnswer
from hiyya_web.hiyya_user.models import VKProfile

from .models import PRODUCT_NEW, PRODUCT_UPCOMING, PRODUCT_ALL, GIFT_STATUS_CHOICES
from .models import Product, Gift, Address

from datetime import datetime, timedelta

UserModel = get_user_model()


def products_list(request, status=PRODUCT_ALL):
    # get list of products based on filter.
    user = request.user

    products = Product.objects.filter_status(status)
    with_main_img = []
    images_url = {}
    for product in products:
        if product.id not in images_url.keys():
            images_url[product.id] = []
        for image in product.images.all().filter(main=False, featured=False):
            images_url[product.id].append(image.image.url)
        if product.images.filter(main=True).exists():
            with_main_img.append(product)
    data = [{'id': p.id,
             'name': p.name,
             'description': p.description,
             'store_url': p.store_url,
             'status': p.status,
             'brand': {'name': p.brand.name if p.brand else 'Unknown',
                       'website': p.brand.website if p.brand else ''},
             'images': images_url[p.id],
             'main_image': p.images.filter(
                 main=True)[0].image.url,
             'watching': True if user.is_authenticated and user in p.notified_users.all() else False,
             'featured': p.featured,
             'featured_image': p.images.filter(featured=True).first().image.url if p.images.filter(
                 featured=True).exists() else ''}
            for p in with_main_img]
    return json_response(data)


@login_required
def notify_me(request, product_id):
    action = request.GET.get('action', 'notify')
    data = {
        'status': 0
    }
    user = request.user
    try:
        product = Product.objects.get(id=product_id)
    except Product.DoesNotExist:
        product = None
    if product:
        if action == 'notify':
            product.notified_users.add(user)
            data['watching'] = 1
        if action == 'unwatch':
            product.notified_users.remove(user)
            data['watching'] = 0
        data['status'] = 1

    return json_response(data)


@login_required
def gift_list(request):
    user = request.user
    data = []
    query = None
    platform_id = None
    if settings.SOCIAL_NETWORK == 'vk':
        query = Gift.objects.filter(Q(receiver=user) | \
                                    Q(receiver_vk__vk_id=user.vk_profile.all()[0].vk_id) | \
                                    Q(sender=user))
    else:
        query = Gift.objects.filter(Q(receiver=user) | \
                                    Q(receiver_fb__facebook_id=user.facebook_id) | \
                                    Q(sender=user))
    for g in query.all():
        try:
            product_image = g.product.images.filter(main=True).get()
        except Product.DoesNotExist:
            product_image = None
        #prepare gift owner question-anwser
        questions = []
        for answer in GiftAnswer.objects.filter(Q(gift=g) & Q(user_id=g.sender_id)):
            questions.append({
                'id': answer.question.pk,
                'text': answer.question.text,
                'choices': map(lambda x: x.strip(), answer.question.choices.split(',')),
                'question_type': answer.question.question_type,
            })

        if settings.SOCIAL_NETWORK == 'vk':
            platform_id = {'vk_id': g.receiver_vk.vk_id if g.sender == user else g.sender.vk_profile.all()[0].vk_id}
        else:
            platform_id = {'fb_id': g.receiver_fb.facebook_id if g.sender == user else g.sender.facebook_id}

        data.append({
            'id': g.id,
            'friend': platform_id,
            'product': {
                'id': g.product.id,
                'name': g.product.name,
                'brand_name': g.product.brand.name if g.product.brand else 'Unknown',
                'image': product_image.image.url if product_image else '',
            },
            'status': 3 if g.sender == user else g.status,
            'sended': True if g.sender == user else False,
            'questions': questions,
            'expired': g.expired,
            'created': str(g.created),
        })

    return json_response(data)


@login_required
def change_gift_status(request, gift_id):
    data = {}
    gift_status = {
        'accept': 2,
        'reject': 5,
    }
    action = request.GET.get('action')
    try:
        gift = Gift.objects.get(pk=gift_id)
    except Gift.DoesNotExists:
        gift = None
    if gift:
        gift.status = gift_status[action]
        gift.save()
        data['status'] = gift.status
    return json_response(data)


@login_required
def make_expired(request, gift_id):
    data = {}
    gift = None
    if settings.SOCIAL_NETWORK == 'vk':
        gift = Gift.objects.get(pk=gift_id, receiver_vk__vk_id=request.user.vk_profile.all()[0].vk_id)
    else:
        gift = Gift.objects.get(pk=gift_id, receiver_fb__facebook_id=request.user.facebook_id)

    if gift:
        now = timezone.now()
        day_ago = now - timedelta(days=1)
        data['gift'] = str(day_ago)
        if day_ago > gift.created:
            gift.expired = True
            gift.status = 4  # expired
            gift.save()
            gift.product.inventory_cnt += 1
            gift.product.save()
            data['status'] = gift.status
    return json_response(data)


@login_required
def gift_send(request):
    js_data = json.loads(request.body)
    user = request.user
    if user.send_limit < 1:
        return json_response({'error': 'send limit reached'})
    product_id = int(str(js_data.get('product_id')))
    message = js_data.get('message')
    receiver_fb_id = None
    receiver_vk_id = None
    platform_user = None
    gift = None

    if settings.SOCIAL_NETWORK == 'vk':
        receiver_vk_id = int(str(js_data.get('receiver_vk_id')))
        name = js_data.get('receiver_vk_name').split(' ')
        first_name = name[0]
        last_name = name[1]
        gender = 'M'
        if js_data.get('receiver_vk_gender') and js_data.get('receiver_vk_gender') == 1:
            gender = 'F'

        platform_user, _ = VKProfile.objects.get_or_create(vk_id=receiver_vk_id,
                                                          defaults={
                                                                    'vk_id': receiver_vk_id,
                                                                    'first_name': first_name,
                                                                    'last_name': last_name,
                                                                    'gender': gender,
                                                          })
        platform_user.save()

        gift = Gift(
            sender=user,
            receiver_vk=platform_user,
            message=message,
            status=1,
            product_id=product_id,
        )
        gift.save()
    else:
        receiver_fb_id = int(str(js_data.get('receiver_fb_id')))
        platform_user, _ = FacebookUser.objects.get_or_create(user_id=user.id, facebook_id=receiver_fb_id,
                                                          defaults={'user_id': user.id,
                                                                    'facebook_id': receiver_fb_id,
                                                                    'name': js_data.get('receiver_fb_name'),
                                                                    'gender': 'F' if js_data.get(
                                                                        'receiver_fb_gender') else 'M',
                                                          })
        platform_user.save()

        gift = Gift(
            sender=user,
            receiver_fb=platform_user,
            message=message,
            status=1,
            product_id=product_id,
        )
        gift.save()

    answers = js_data.get('answers')
    for a in answers:
        answer = GiftAnswer(
            question_id=int(str(a[u'question_id'])),
            gift=gift,
            answer=a[u'answer'],
            user_id=user.pk,
        )
        answer.save()
    gift.product.inventory_cnt -= 1
    gift.product.save()
    user.send_limit -= 1
    user.save()

    return json_response({'id': gift.id, 'send_limit': user.send_limit})


@login_required
def gift_accept_survey(request):
    js_data = json.loads(request.body)
    user = request.user
    if user.receive_limit < 1:
        return json_response({'error': 'receive limit reached'})

    gift = Gift.objects.filter(Q(id=js_data.get('gift_id'))).get()

    answers = js_data.get('answers')
    for a in answers:
        answer = GiftAnswer(
            question_id=int(str(a[u'question_id'])),
            gift=gift,
            answer=a[u'answer'],
            user_id=user.pk,
        )
        answer.save()

    sender_answers_query = GiftAnswer.objects.filter(Q(gift=gift) & Q(user=gift.sender))

    sender_answers = []
    for sa in sender_answers_query.all():
        sender_answers.append({
            'question_id': sa.question.pk,
            'answer': sa.answer,
        })
    user.receive_limit -= 1
    user.save()

    return json_response({'receive_limit': user.receive_limit, 'sender_answers': sender_answers})


@login_required
def gift_add_address(request):
    js_data = request.POST
    gift_id = js_data.get('gift_id')
    gift = Gift.objects.get(id=gift_id)
    address, _ = Address.objects.get_or_create(gift=gift,
                                               defaults={'name': js_data['name'],
                                                         'address': js_data['address'],
                                                         'phone': js_data['phone'],
                                                         'city_st': js_data['city_st'],
                                                         'zip_code': js_data.get('zip_code')})
    address.save()
    return json_response({})


def to_product_redirect(request, product_id):
    return HttpResponseRedirect('/#/product/details/' + product_id)
