"""
Django settings for hiyya_web project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
print BASE_DIR


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '5jklvxa%n$i08b5i6iinnb3o$-6%85&mxahf1#*ha!jhc42h(+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

TEMPLATE_DIRS = (
    BASE_DIR + '/templates/',
)

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_facebook',
    'south',
    'storages',
    'sslserver',

    'hiyya_web.hiyya_user',
    'hiyya_web.survey',
    'hiyya_web.product',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'hiyya_web.urls'

WSGI_APPLICATION = 'hiyya_web.wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'django_facebook.context_processors.facebook',
)

AUTHENTICATION_BACKENDS = (
    'django_facebook.auth_backends.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_USER_MODEL = 'hiyya_user.User'

FACEBOOK_STORE_FRIENDS = False
# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
ON_OPENSHIFT = True if os.getenv('OPENSHIFT_GEAR_NAME', False) else False

if ON_OPENSHIFT:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.getenv('OPENSHIFT_APP_NAME'),
            'USER': os.getenv('OPENSHIFT_POSTGRESQL_DB_USERNAME'),
            'PASSWORD': os.getenv('OPENSHIFT_POSTGRESQL_DB_PASSWORD'),
            'HOST': os.getenv('OPENSHIFT_POSTGRESQL_DB_HOST'),
            'PORT': os.getenv('OPENSHIFT_POSTGRESQL_DB_PORT')
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_DIR = os.path.join(BASE_DIR, 'static')

if ON_OPENSHIFT:
    STATIC_ROOT = os.path.join(BASE_DIR, 'static')
else:
    STATICFILES_DIRS =(STATIC_DIR,)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.getenv('OPENSHIFT_DATA_DIR', BASE_DIR), 'images')

FACEBOOK_APP_ID = 1404133173154451
FACEBOOK_APP_SECRET = '805bfd8d2b590b8f98a770ca017d3d5f'
FACEBOOK_DEFAULT_SCOPE = ['basic_info', 'user_friends', 'email', 'user_birthday', 'user_work_history',
                          'user_education_history', 'user_hometown', 'user_location', 'user_likes',
                          'friends_birthday', 'friends_work_history', 'friends_education_history',
                          'friends_location', 'friends_likes', 'publish_actions']

VK_APP_ID = 4223130
VK_APP_SECRET = 'kVIgrrgg4dP0039JXqi4'

SOCIAL_NETWORK = 'vk'

BROKER_URL = 'django://'

INSTALLED_APPS += ('kombu.transport.django', )

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'crchemist@gmail.com'
EMAIL_HOST_PASSWORD = 'freelancer'
EMAIL_USE_TLS = True

'''
if ON_OPENSHIFT:
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    AWS_ACCESS_KEY_ID='AKIAI6RDZQT4VBG5FJ6A'
    AWS_SECRET_ACCESS_KEY='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'
    AWS_STORAGE_BUCKET_NAME = 'hiyya'

    from boto.s3.connection import S3Connection
    AWS_CALLING_FORMAT = S3Connection.DefaultCallingFormat
'''

try:
    from hiyya_web.settings_local import *
except ImportError:
    print 'No local settings found.'
