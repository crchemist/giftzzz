from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    (r'^facebook/', include('django_facebook.urls')),
    (r'^accounts/', include('django_facebook.auth_urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^product/', include('hiyya_web.product.urls')),
    url(r'^survey/', include('hiyya_web.survey.urls')),
    url(r'', include('hiyya_web.hiyya_user.urls')),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
        document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
