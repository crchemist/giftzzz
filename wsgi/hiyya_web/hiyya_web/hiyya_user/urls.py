from django.conf.urls import patterns, include, url
urlpatterns = patterns('hiyya_web.hiyya_user.views',
    url(r'^$', 'home'),
    url(r'info/', 'user_info'),
    url(r'logout/', 'user_logout'),
    url(r'platform-friends/', 'platform_friends_list'),
    url(r'vk_login/', 'vk_login', name='vk_login'),
    url(r'user/vk_calback/', 'vk_calback', name='vk_calback'))
