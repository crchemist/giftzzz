from django.db import models

from django.contrib.auth.models import AbstractUser, UserManager

from django_facebook.models import FacebookModel


class User(AbstractUser, FacebookModel):
    """Stores all user-related data.
    """
    objects = UserManager()

    state = models.CharField(max_length=255, blank=True, null=True)
    send_limit = models.IntegerField(default=3)
    receive_limit = models.IntegerField(default=3)


class VKProfile(models.Model):
    user = models.ForeignKey(User, related_name='vk_profile', blank=True, null=True)
    vk_access_token = models.TextField(
        blank=True, help_text='VK token for offline access', null=True)
    vk_id = models.BigIntegerField(blank=True, unique=True, null=True)
    first_name = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(choices=(
        ('F', 'female'), ('M', 'male')), blank=True, null=True, max_length=1)
    picture = models.URLField(blank=True, null=True,)
