from django.contrib import admin

from .models import User, VKProfile

admin.site.register(User)
admin.site.register(VKProfile)
