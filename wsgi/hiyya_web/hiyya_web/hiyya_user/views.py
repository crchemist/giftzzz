import json
import requests

import vkontakte
from datetime import datetime

from django.conf import settings

import urllib2
from django_facebook.api import FacebookUserConverter

from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from hiyya_web.hiyya_user.models import VKProfile, User


from hiyya_web.utils import json_response


def home(request):
    return render_to_response('index.html', {})


def user_logout(request):
    logout(request)
    return json_response({'anonymous': True})


def user_info(request):
    user = request.user
    data = {'anonymous': True}
    if user.is_anonymous() or user.is_superuser:
        return json_response(data)

    if settings.SOCIAL_NETWORK == 'vk':
        data.update({
            'send_limit': user.send_limit,
            'receive_limit': user.receive_limit,
            'name': "%s %s" % (user.vk_profile.all()[0].first_name, user.vk_profile.all()[0].last_name),
            'photo': user.vk_profile.all()[0].picture if user.vk_profile.all()[0].picture else '',
            'vk_id':  user.vk_profile.all()[0].vk_id,
            'anonymous': False})
    else:
        data.update({
            'send_limit': user.send_limit,
            'name': user.facebook_name,
            'receive_limit': user.receive_limit,
            'photo': user.image.url if user.image else '',
            'anonymous': False})

    return json_response(data)


@login_required
def fb_friends_list(request):
    '''Get list of friends.
    '''
    user = request.user
    data = []
    for f in user.friends():
        data.append({
            'name': f.name, 'id': f.id,
            'fb_id': f.facebook_id})

    return json_response(data)


@login_required
def vk_friends_list(request):
    '''Get list of friends.
    '''
    user = request.user
    data = [{
            'name': "%s %s" % (f.first_name, f.last_name), 'id': f.id,
            'fb_id': f.vk_id,
            'photo': f.picture} for f in user.vk_friends.all()]
    return data


@login_required
def platform_friends_list(request):
    data = None
    if settings.SOCIAL_NETWORK == 'vk':
        data = vk_friends_list(request)
    else:
        data = fb_friends_list(request)
    return json_response(data)


def vk_login(request):
    url = "http://oauth.vk.com/authorize?client_id=%s&scope=wall,photos&redirect_uri=https://%s/user/vk_calback/&display=page&v=5.5&response_type=code" % (settings.VK_APP_ID, request.get_host())
    return redirect(url)


def vk_calback(request):
    code = None
    access_token = None
    try:
        code = request.GET.get('code')
    except:
        pass
    if code:
        url = "https://oauth.vk.com/access_token?client_id=%s&client_secret=%s&code=%s&redirect_uri=https://%s/user/vk_calback/" % (settings.VK_APP_ID, settings.VK_APP_SECRET, code, request.get_host())
    else:
        return HttpResponseBadRequest('There is not code!')

    resp = requests.get(url)
    try:
        access_token = resp.json()['access_token']
    except:
        pass

    try:
        user_id = resp.json()['user_id']
    except:
        pass

    user = None
    profile = None

    if access_token and user_id:
        vk = vkontakte.API(token=access_token)
        details = vk.get('users.get', uids=user_id, fields='photo_100,sex,bdate,city,country,screen_name')[0]

        try:
            profile = VKProfile.objects.get(vk_id=user_id)
            if profile:
                user = profile.user
        except VKProfile.DoesNotExist:
            pass

        fake_password = FacebookUserConverter._generate_fake_password()
        if not user:
            user = User.objects.create(username='vk_' + str(user_id))
            user.set_password(fake_password)
            user.first_name = details['first_name'][:30]
            user.last_name = details['last_name'][:30]
            user.is_staff = False
            user.save()
            if not profile:
                try:
                    profile = VKProfile.objects.create(user=user, first_name=details['first_name'][:30], last_name=details['last_name'][:30])
                    profile.vk_id = user_id
                    profile.vk_access_token = access_token
                    if details.get('bdate'):
                        try:
                            profile.date_of_birth = datetime.strptime(details['bdate'], '%d.%m.%Y').strftime('%Y-%m-%d')
                        except ValueError:
                            if len(details['bdate'].split('.')) == 2:
                                bdate = details['bdate'] + '.1900'
                                profile.date_of_birth = datetime.strptime(bdate, '%d.%m.%Y').strftime('%Y-%m-%d')
                            else:
                                pass
                    if details.get('photo_100'):
                        profile.picture = details['photo_100']
                    if details.get('sex'):
                        profile.gender = 'm'
                        if details['sex'] == 1:
                            profile.gender = 'f'
                    profile.save()
                # need to delete user to prevent errors later
                except Exception:
                    user.delete()
            else:
                profile.vk_access_token = access_token
                profile.user = user
                profile.save()
        else:
            user.set_password(fake_password)
            if profile.vk_access_token != access_token:
                profile.vk_access_token = access_token
            profile.save()
            user.save()
    user = authenticate(username=user.username, password=fake_password)
    if user is not None and user.is_active:
        login(request, user)

    return redirect('/')
