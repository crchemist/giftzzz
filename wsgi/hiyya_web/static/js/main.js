$(function () {
    ko.bindingHandlers.href = {
        init: function (el, valueAccessor) {
            $(el).attr('href', valueAccessor);
        }
    };
    ko.bindingHandlers.counter = {
        update: function (el, valueAccessor) {
            var counter = ko.unwrap(valueAccessor());
            $(el).html(counter);
            counter > 0 ? $(el).show() : $(el).hide();
        }
    };

    var APP_ID = 4577480;

    var Question = function (data) {
        data = data || {};
        this.id = data.id;
        this.text = data.text;
        this.question_type = data.question_type;
        this.choices = data.choices;
        this.user_answer = ko.observable();
        this.answer_select = this.answer_select.bind(this);
        this.is_answered = ko.computed(function () {
            return !!ko.unwrap(this.user_answer)
        }.bind(this))

        this.matched = ko.observable(undefined);
        this.match_answer = this.match_answer.bind(this)
        this.render_answer_comparsion = this.render_answer_comparsion.bind(this)
        this.pretify_answer = this.pretify_answer.bind(this)
    }

    ko.utils.extend(Question.prototype, {
        answer_select: function (answer) {
            if (this.question_type === 'select-multiple') {
                var answers = ko.unwrap(this.user_answer) ? ko.unwrap(this.user_answer).split(',') : [];
                if (ko.utils.arrayIndexOf(answers, answer) !== -1) {
                    ko.utils.arrayRemoveItem(answers, answer);
                    this.user_answer(answers.join(','));
                } else {
                    answers.push(answer);
                    this.user_answer(answers.join(','));
                }
            } else {
                this.user_answer(answer)
            }
        },
        template_name: function () {
            return  'question-' + this.question_type + '-tmpl'
        },
        match_answer: function (answer) {
            if (ko.unwrap(this.matched) == undefined) {
                switch (this.question_type) {
                    case 'radio':
                    case 'select':
                        this.matched(this.user_answer() == answer);
                        break;
                    case 'integer':
                    case 'text':
                        this.matched($.trim(this.user_answer()) == $.trim(answer));
                        break;
                    case 'select-multiple':
                        var question = this,
                            user_answers = question.user_answer().split(','),
                            sender_answers = answer.split(',');

                        ko.utils.arrayForEach(sender_answers, function (sender_answer) {
                            if (ko.utils.arrayIndexOf(user_answers, sender_answer) == -1)
                                question.matched(false);
                        });
                        question.matched() !== false && question.matched(sender_answers.length == user_answers.length);
                        break;
                }
            }
            return this.matched();
        },
        render_answer_comparsion: function (answer) {
            var check = this.match_answer(answer)
            var css_class = check ? 'text-green' : 'text-red';
            return '<span class="' + css_class + '">' +
                this.pretify_answer(this.user_answer()) + '</span><span>' +
                this.text + '</span><span class="' + css_class + '">' +
                this.pretify_answer(answer) + '</span>'
        },
        letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'],
        pretify_answer: function (answer) {
            switch (this.question_type) {
                case 'radio':
                case 'select':
                    return this.letters[ko.utils.arrayIndexOf(this.choices, answer)];
                case 'integer':
                case 'text':
                    return $.trim(answer)
                case 'select-multiple':
                    var question = this,
                        out = [], index;
                    ko.utils.arrayForEach(answer.split(','), function (a) {
                        index = ko.utils.arrayIndexOf(question.choices, a)
                        if (index >= 0)
                            out.push(question.letters[index]);
                    });
                    return out.join(', ');
            }
        }
    })

    var Survey = function (data, after_survey_steps_count) {
        data = data || {};
        this.thumbnail = ko.observable();
        this.after_survey_steps_count = after_survey_steps_count || 0;
        this.questions = ko.observableArray([]);

        this.current_question = ko.observable();
        this.current_question_index = ko.computed(function () {
            return this.questions().indexOf(ko.unwrap(this.current_question)) + 1 || 0;
        }, this);
        this.questions_count = ko.computed(function () {
            return this.questions().length;
        }.bind(this));
        this.matched_count = ko.computed(function () {
            var count = 0;
            ko.utils.arrayForEach(this.questions(), function (q) {
                q.matched() && count++;
            });
            return count;
        }.bind(this));
        this.progress = ko.computed(function () {
            return this.current_question_index() * 100 / (this.questions_count() + this.after_survey_steps_count);
        }.bind(this));

        this.answers = [];
        this.process_survey = function (question, event) {
            this.answers.push({
                question_id: this.current_question().id,
                answer: this.current_question().user_answer()
            })
            if (this.current_question_index() < this.questions_count()) {
                this.current_question(this.questions()[this.current_question_index()]);
            } else {
                $(event.target).trigger('survey_ended', this)
            }
        }.bind(this);

        this.populate = this.populate.bind(this);
        this.populate(data);
        this.get_question = this.get_question.bind(this);
        this.current_question(this.questions()[0]);

    }

    ko.utils.extend(Survey.prototype, {
        populate: function (data) {
            data.questions && ko.utils.arrayForEach(data.questions, function (question) {
                this.questions.push(new Question(question));
            }.bind(this));
        },
        get_question: function (id) {
            return ko.utils.arrayFirst(this.questions(), function (q) {
                return q.id == id;
            })
        }
    });

    // modals
// products
    var $invite_wizard = $('#invite-wizard').wizard({contentHeight: 525, contentWidth: 640});
    var $accept_wizard = $("#accept-wizard").wizard({contentHeight: 525, contentWidth: 640});
    var $product_modal = $('#product-detail')

    $product_modal.on('hidden.bs.modal', function (e) {
        if ($invite_wizard.modal.data('from_detail') !== true)
            window.history && window.history.back();
    });

    $accept_wizard.on('hidden.bs.modal', function (e) {
        $accept_wizard.dialog.off('survey_ended');
    });
    $invite_wizard.modal.on('show.bs.modal',function (e) {
        if ($product_modal.data('bs.modal') && $product_modal.data('bs.modal').isShown) {
            $invite_wizard.modal.data('from_detail', true);
            $product_modal.modal('hide');
        } else {
            $invite_wizard.modal.data('from_detail', false);
        }
    }).on('hidden.bs.modal', function (e) {
        $invite_wizard.dialog.off('survey_ended');
        ko.contextFor(e.target).$root.user().friends_filter('');
        if (window.history) {
            if ($invite_wizard.modal.data('from_detail'))
                window.history.go(-2);
            else
                window.history.back();

        }
    });

    var Product = function (data) {
        data = data || {};
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.store_url = data.store_url;
        this.status = data.status;
        this.brand = data.brand;
        this.main_image = data.main_image;
        this.images = data.images.concat([data.main_image]);

        this.watching = ko.observable(data.watching);
        this.featured = data.featured;
        this.featured_image = data.featured_image;

        this.notify = this.notify.bind(this);
    }

    ko.utils.extend(Product.prototype, {
        notify: function (product, event) {
            var action = product.watching() ? 'unwatch' : 'notify';
            $.getJSON('/product/notify_me/' + product.id + '/?action=' + action, function (response) {
                if (response['status'] === 1) {
                    product.watching(!!response['watching']);
                }
            })
        },
        show: function () {
            console.log(arguments)
            $product_modal.modal('show')

            $('#carousel-product').carousel();
        }
    });


    // gifts

    var GIFT_STATUSES = {
        1: 'new',
        2: 'accepted',
        3: 'sent',
        4: 'expired',
        5: 'rejected'
    }
    var Gift = function (data) {
        data = data || {};
        this.id = data.id;
        this.friend = undefined;
        this.product = data.product;
        this.status = ko.observable(GIFT_STATUSES[data.status]);
        this.sended = data.sended;
        this.created = new Date(data.created)

        this.expired = new Date(this.created.getTime() + (1000 * 3600 * 24)).getTime() < (new Date()).getTime();
        if (!this.sended && this.expired)
            this.status('expired');
        this.survey = ko.observable(new Survey(data, 2));

        this.survey().thumbnail(data.product.image);

        this.result_text = ko.computed({
            read: function () {
                return 'Ви отримали ' + this.survey().matched_count() + '/' + this.survey().questions().length;
            }.bind(this),
            deferEvaluation: true
        });
        this.result_answers = ko.observableArray([]);
        this.wizard = undefined;
        this.process_response = this.process_response.bind(this)
        this.accept_address = this.accept_address.bind(this)
        this.accept_address_form = this.accept_address_form.bind(this)
        this.process_accept = this.process_accept.bind(this)
        this.match_answers = this.match_answers.bind(this)


        this.mouseover = this.mouseover.bind(this)
        this.mouseout = this.mouseout.bind(this)
        this.change_expire_time = this.change_expire_time.bind(this)
        this.make_expired = this.make_expired.bind(this)

        this.expire_time = ko.observable('');
    }

    ko.utils.extend(Gift.prototype, {
        change_expire_time: function () {
            var diff, hours, minutes, seconds;
            if (this.expired) {
                diff = Math.ceil(((new Date()).getTime() - (this.created.getTime() + 24 * 3600000)) / 1000);
                hours = Math.floor(diff / 3600);
                minutes = Math.floor((diff - hours * 3600) / 60);
                seconds = Math.floor(diff - hours * 3600 - minutes * 60);
                this.expire_time('Прорзиція закінчилася ' + ('00' + hours).slice(-2) + ':' + ('00' + minutes).slice(-2) + ':' + ('00' + seconds).slice(-2));
            } else {
                diff = Math.ceil((this.created.getTime() + 24 * 3600000 - (new Date()).getTime()) / 1000);
                hours = Math.floor(diff / 3600);
                minutes = Math.floor((diff - hours * 3600) / 60);
                seconds = Math.floor(diff - hours * 3600 - minutes * 60);
                this.expire_time('Пропозиція закінчується в ' + ('00' + hours).slice(-2) + ':' + ('00' + minutes).slice(-2) + ':' + ('00' + seconds).slice(-2));
            }
        },
        interval: undefined,
        mouseover: function () {
            var self = this;
            self.change_expire_time();
            this.interval = setInterval(self.change_expire_time, 1000);
        },
        mouseout: function () {
            clearInterval(this.interval);
        },
        process_accept: function () {
            var gift = this;
            gift.wizard = $accept_wizard;
            gift.wizard.reset().show();
            gift.wizard.dialog.on('survey_ended', function () {
                gift.match_answers();
                gift.wizard.nextButton.click()
            })
        },
        next_step: function (gift, event) {
            gift.wizard.nextButton.click()
        },
        process_response: function (sender_answers) {
            var gift = this;
            ko.utils.arrayForEach(sender_answers, function (sa) {
                var question = gift.survey().get_question(sa.question_id);
                var compared = question.match_answer(sa.answer)
                gift.result_answers.push({
                    'text': question.render_answer_comparsion(sa.answer)
                })
            })
        },
        accept_address_form: function () {
            this.wizard.hide();
            $('#accept-address-form').modal();
        },
        accept_address: function () {
            var gift = this;
            var $form = $('#accept-gift-address-form');
            $.post('/product/gift-add-address/', {
                name: $form.find('[name=name]').val(),
                address: $form.find('[name=address]').val(),
                gift_id: gift.id,
                city_st: $form.find('[name=city_st]').val(),
                zip_code: $form.find('[name=zip_code]').val(),
                phone: $form.find('[name=phone]').val()
            }).success(function () {
                $.getJSON('product/gift/' + gift.id + '/?action=accept', function (response) {
                    if (response['status']) {
                        gift.status(GIFT_STATUSES[response['status']]);
                    }
                    var user = ko.contextFor($form[0]).$root.user();
                    user.receive_limit(user.receive_limit() - 1);
                    $('#accept-address-form').modal('hide')
                })
            })
        },
        match_answers: function () {
            var gift = this;
            $.ajax({
                type: "POST",
                url: '/product/gift-accept-survey/',
                processData: false,
                contentType: 'application/json',
                data: JSON.stringify({
                    gift_id: gift.id,
                    answers: gift.survey().answers}),
                success: function (response) {
                    gift.process_response(response.sender_answers)
                }
            })
        },
        reject: function (gift) {
            $.getJSON('product/gift/' + gift.id + '/?action=reject', function (response) {
                if (response['status']) {
                    gift.status(GIFT_STATUSES[response['status']]);
                }
            })
        },
        make_expired: function(){
            this.expired = true;
            this.status('expired');
            $.getJSON('product/gift/expired/' + this.id)
        }
    });

    var SendGift = function (product) {
        var send_gift = this;
        send_gift.product = product;
        send_gift.survey = ko.observable();

        send_gift.start = send_gift.start.bind(this);
        send_gift.close = send_gift.close.bind(this);
        send_gift.next = send_gift.next.bind(this);
        $.when($.getJSON('/survey/?product_id=' + product.id, function (data) {
                send_gift.survey(new Survey(data, 2));
                send_gift.survey().thumbnail(product.main_image);
            })).then(function () {
            send_gift.wizard = $invite_wizard;
            send_gift.wizard.dialog.on('survey_ended', function () {
                send_gift.next();
            });

            send_gift.start();
        });
    };
    ko.utils.extend(SendGift.prototype, {
        close: function () {
            this.wizard.reset().hide();
        },
        next: function () {
            this.wizard.nextButton.click();
        },
        start: function () {
            this.wizard.reset().show();
        }
    })

    var User = function () {
        var user = this;
        user.send_limit = ko.observable();
        user.receive_limit = ko.observable();
        user.name = ko.observable();
        user.logged = ko.observable();
        user.photo = ko.observable();
        ButtonLock = ko.observable(false);

        user.friends = ko.observableArray([])

        user.loaded = $.Deferred();
        user.get_friend = function (id) {
            return ko.utils.arrayFirst(user.friends(), function (friend) {
                return friend.vk_id == id;
            })
        }
        $.when($.getJSON('/user/info')).then(function (info) {
            if (!info.anonymous) {
                //var facebook_auth = FB.getAuthResponse() || {};
                //if (facebook_auth.accessToken) {
                //    user.send_limit(info.send_limit);
                //    user.receive_limit(info.receive_limit);
                //    user.photo('https://graph.facebook.com/' + facebook_auth.userID + '/picture?type=large&return_ssl_results=1&width=100&height=100');
                //    user.name(info.name)
                //    FB.api('/me/friends', {fields: 'name, id, gender'}, function (response) {
                //        if (response.data) {
                //            $.each(response.data, function (index, friend) {
                //                user.friends.push({
                 //                   'gender': friend.gender,
                 //                   'name': friend.name,
                 //                   'fb_id': friend.id,
                 //                   'avatar': 'https://graph.facebook.com/' + friend.id + '/picture?type=large&return_ssl_results=1&width=100&height=100'
                 //               })
                 //           });
                 //       }
                 //   });
                //
                user.send_limit(info.send_limit);
                user.receive_limit(info.receive_limit);
                user.photo(info.photo);
                user.name(info.name)
                VK.Api.call('friends.get', {uid: info.vk_id, fields:('first_name, last_name, sex, photo_100')}, function(r) {
                    if(r.response) {
                        $.each(r.response, function (index, friend) {
                                user.friends.push({
                                    'gender': friend.sex,
                                    'name': friend.first_name+" "+friend.last_name,
                                    'vk_id': friend.uid,
                                    'avatar': friend.photo_100
                                })
                            });
                    }
                });
                user.logged(true)
            } else {
                user.logged(false)
            }
            user.loaded.resolve();
        })

        user.selected_friend = ko.observable();
        user.friends_filter = ko.observable();
        user.friends_filtered = ko.computed(function () {
            if (!user.friends_filter()) {
                return user.friends();
            } else {
                return ko.utils.arrayFilter(user.friends(), function (friend) {
                    return friend.name.toLowerCase().indexOf(user.friends_filter().toLowerCase()) !== -1;
                });
            }
        });
        user.friend_select = function (friend) {
            user.selected_friend(friend);
            user.send_gift() && user.send_gift().next();
        }

        user.send_gift = ko.observable();
        user.start_send = function (product) {
            user.send_gift(new SendGift(product));
        };
        user.send_gift_submit = function (data) {
            var friend = user.selected_friend(),
                product = user.send_gift().product,
                message = $('#gift-message').val();
            VK.Api.call('wall.post', {owner_id: friend.vk_id, attachments:('photo66748_265827614','http://demo.giftzzz.com.ua/'),
                                              message:'Привіт! Я подарув тобі '+ product.name +' від '+product.store_url+' ! Щоб отримати зайди на demo.giftzzz.com.ua/#/my-gifts/ . Але часу щоб забрати маєш 24 години!'},
                                                function(r) {
                                                    if(r.response) {
                                                        user.send_gift_ajax(friend, product, message)
                                                    } else{
                                                        user.send_gift_submit()
                                                    }
                                            });
            ButtonLock(true);

        };
        user.send_gift_ajax = function (friend, product, message){
            $.ajax({
                            type: "POST",
                            url: '/product/gift-send/',
                            processData: false,
                            contentType: 'application/json',
                            data: JSON.stringify({
                                product_id: product.id,
                                receiver_vk_id: friend.vk_id,
                                receiver_vk_name: friend.name,
                                receiver_vk_gender: friend.gender,
                                message: message,
                                answers: user.send_gift().survey().answers
                            }),
                            success: function (response) {
                                //var base_url = location.protocol + '//' + location.host + '/';
                                //FB.api(
                                //    'me/objects/giftzzz:gift',
                                //    'post',
                                //    {
                                //        app_id: APP_ID,
                                //        object: {
                                //            url: base_url + 'product/details/' + product.id,
                                //            title: product.name,
                                //            image: base_url + product.main_image,
                                //            description: message}
                                //    },
                                //    function (response) {
                                //        console.log(response)
                                //        FB.api(
                                //            'me/giftzzz:gave',
                                //            'post',
                                //            {
                                //                gift: response.id,
                                //                tags: friend.fb_id
                                //            },
                                //            function (response) {
                                //                console.log(response)
                                //            }
                                //        );
                                //    }
                                //);
                                console.log('Gift sent ' + response.id);
                                user.send_limit(response.send_limit);
                                user.send_gift().next();
                            }
                        })
                ButtonLock(false);
            }
        //events
        user.is_able_to_send_a_gift = ko.computed(function () {
            return !user.logged() || (user.send_limit() != 0 && user.friends().length > 0);
        });
        user.send_gift_mouseover = function (product, event) {
            if (!user.is_able_to_send_a_gift()) {
                var message = user.friends().length == 0 ? 'Почекайте поки збереться список ваших друзів' : 'Ви не має безплатних подарунків щоб подарувати друзям';
                $(event.target).tooltip({
                    title: message
                }).addClass('btn-faded').tooltip('show').on('click.send', function (e) {
                    e.preventDefault();
                });
            } else {
                $(event.target).tooltip('hide').removeClass('btn-faded').off('click.send')
            }
        };
        user.send_gift_mouseout = function (product, event) {
            $(event.target).tooltip('hide').removeClass('btn-faded').off('click.send');
        };
        user.is_able_to_accept_a_gift = ko.computed(function () {
            return (user.receive_limit() != 0 && user.friends().length > 0);
        });
        user.accept_gift_mouseover = function (product, event) {
            if (!user.is_able_to_accept_a_gift()) {
                var message = user.friends().length == 0 ? 'Почекайте поки збереться список ваших друзів' : 'Ви не має безплатних подарунків щоб подарувати друзям';
                $(event.target).tooltip({
                    title: message
                }).addClass('btn-faded').tooltip('show').on('click.send', function (e) {
                    e.preventDefault();
                });
            } else {
                $(event.target).tooltip('hide').removeClass('btn-faded').off('click.send')
            }
        }
        user.accept_gift_mouseout = function (product, event) {
            $(event.target).tooltip('hide').removeClass('btn-faded').off('click.send');
        }

    }

    function ProductViewModel(facebook_data) {
        var self = this;
        app = self;

        self.loading = ko.observable(true);
        self.products = ko.observableArray([]);
        self.selected_product = ko.observable();
        self.main_slider_products = ko.computed(function () {
            return ko.utils.arrayFilter(self.products(),function (p) {
                return p.featured;
            }).slice(0, 2)
        });

        self.gifts = ko.observableArray([]);
        self.selected_gift = ko.observable();

        self.user = ko.observable();

        self.get_product = function (id) {
            return ko.utils.arrayFirst(self.products(), function (p) {
                return p.id == id;
            });
        }
        self.get_gift = function (id) {
            return ko.utils.arrayFirst(self.gifts(), function (g) {
                return g.id == id;
            });
        }

        // counters
        self.get_count_by_status = function (data, status) {
            var count = 0;
            ko.utils.arrayForEach(data, function (p) {
                (ko.unwrap(p.status) === status) && count++;
            });
            return count;
        };

        // config
        self.config = {
            pages: {
                main: {
                    'new': {
                        url: '/#/new/',
                        status: 'new',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.products(), 'new');
                        }),
                        actions: ['action-send-to-friend-tmpl']

                    },
                    upcoming: {
                        url: '/#/upcoming/',
                        status: 'upcoming',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.products(), 'upcoming');
                        }),
                        actions: ['action-visit-store-tmpl']
                    },
                    expired: {
                        url: '/#/expired/',
                        status: 'expired',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.products(), 'expired');
                        }),
                        actions: ['action-notify-me-tmpl']
                    }
                },
                gifts: {
                    'new': {
                        url: '/#/my-gifts/new/',
                        status: 'new',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.gifts(), 'new');
                        }),
                        actions: [
                            'action-accept-gift-tmpl',
                            'action-reject-gift-tmpl'
                        ]
                    },
                    'accepted': {
                        url: '/#/my-gifts/accepted/',
                        status: 'accepted',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.gifts(), 'accepted');
                        }),
                        actions: []
                    },
                    'sent': {
                        url: '/#/my-gifts/sent/',
                        status: 'sent',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.gifts(), 'sent');
                        }),
                        actions: []
                    },
                    'expired': {
                        url: '/#/my-gifts/expired/',
                        status: 'expired',
                        count: ko.computed(function () {
                            return self.get_count_by_status(self.gifts(), 'expired');
                        }),
                        actions: []
                    }
                }
            }
        }

        // navigation stuff
        self.state = ko.observable(); // 'front-page' or 'my-gifts'

        self.front_page_tab_name = ko.observable();
        self.front_page_tab_actions = [];
        self.front_page_tab_name.subscribe(function (status) {
            self.front_page_tab_actions = self.config.pages.main[status].actions;
        })
        self.front_page_tab = function (status) {
            self.state('front-page');
            self.front_page_tab_name(status);
        };
        self.front_page_tab_products = ko.computed(function () {
            return ko.utils.arrayFilter(self.products(), function (p) {
                return p.status === ko.unwrap(self.front_page_tab_name)
            })
        })

        self.my_gifts_tab_name = ko.observable();
        self.my_gifts_tab_actions = [];
        self.my_gifts_tab_name.subscribe(function (status) {
            self.my_gifts_tab_actions = self.config.pages.gifts[status].actions;
        });
        self.my_gifts_tab = function (status) {
            self.state('my-gifts');
            self.my_gifts_tab_name(status);
        };
        self.my_gifts_tab_items = ko.computed(function () {
            return ko.utils.arrayFilter(self.gifts(), function (p) {
                return ko.unwrap(p.status) === ko.unwrap(self.my_gifts_tab_name)
            })
        });

        // --------------


        self.products_update = function () {
            var status = 'all';
            return $.getJSON('/product/list/' + status + '/', function (response) {
                ko.utils.arrayForEach(response, function (product) {
                    self.products.push(new Product(product));
                });
            });
        };

        self.gifts_expire_interval = undefined;
        self.gifts_expire_worker = function () {
            var now = (new Date()).getTime(),
                counter = 0;
            ko.utils.arrayForEach(self.gifts(), function (gift) {
                if (ko.unwrap(gift.status).search(/new|accepet/) !== -1) {
                    if (new Date(gift.created.getTime() + (1000 * 3600 * 24)).getTime() < now) {
                        gift.make_expired();
                    }
                    counter++
                }
            })
            if (counter == 0) {
                clearInterval(self.gifts_expire_interval);
            }
        };

        self.gifts_update = function () {
            $.getJSON('/product/gift/', function (response) {
                ko.utils.arrayForEach(response, function (data) {
                    var gift = new Gift(data);
                    gift.friend = self.user().get_friend(data.friend.vk_id)
                    self.gifts.push(gift);
                });
                self.gifts_expire_interval = setInterval(self.gifts_expire_worker, 1000);
            });
        };

        self.flash_message = ko.observable()
        self.flash_message_remove = function () {
            self.flash_message(undefined);
        };

        self.init_front_page = function () {
            if (!self.initialized_front_page) {
                console.log('Enter front page');
                $.when(self.products_update()).then(function () {
                    $('#carousel').carousel()
                    self.initialized_front_page = true;
                });

            }
        };

        self.init_my_gifts = function () {
            if (!self.initialized_my_gifts) {
                self.gifts_update();
                self.initialized_my_gifts = true;
            }
        };

        self.user(new User());
        self.user().loaded.done(function () {
            Sammy(function () {
                this.before({except: {path: '#_=_'}}, function (context) {
                    $.cookie('redirect', context.path)
                });
                this.get('#/', function () {
                    self.init_front_page();
                    self.front_page_tab(self.config.pages.main.new.status)
                })
                this.get(self.config.pages.main.new.url, function (context) {
                    self.init_front_page();
                    self.front_page_tab(self.config.pages.main.new.status)
                });
                this.get(self.config.pages.main.upcoming.url, function (context) {
                    self.init_front_page();
                    self.front_page_tab(self.config.pages.main.upcoming.status)
                });
                this.get(self.config.pages.main.expired.url, function (context) {
                    self.init_front_page();
                    self.front_page_tab(self.config.pages.main.expired.status)
                });

                this.get('#/my-gifts/', function () {
                    self.my_gifts_tab(self.config.pages.gifts.new.status)
                });

                this.get(self.config.pages.gifts.new.url, function () {
                    self.my_gifts_tab(self.config.pages.gifts.new.status)
                })
                this.get(self.config.pages.gifts.accepted.url, function () {
                    self.my_gifts_tab(self.config.pages.gifts.accepted.status)
                });
                this.get(self.config.pages.gifts.sent.url, function () {
                    self.my_gifts_tab(self.config.pages.gifts.sent.status)
                });
                this.get(self.config.pages.gifts.expired.url, function () {
                    self.my_gifts_tab(self.config.pages.gifts.expired.status)
                });
                this.before(/#\/my-gifts\//, function (context) {
                    console.log('Enter my gifts')
                    if (!self.user().logged()) {
                        //var $form = $('#facebook-login');
                        var $form = $('#vk-login');
                        $form.submit();
                        return false;
                    }
                    self.init_my_gifts();
                });


                this.get('#/user/logout/', function (context) {
                    console.log('Logout user')
                    self.user().logged(false)
                    //@todo  user_data undefined
                    $.getJSON('/user/logout', function (response) {
                        context.redirect('#/');
                    })
                });

                this.get('#/user/send-gift/:product_id/', function (context) {
                    if (!self.user().logged()) {
                        //$('#facebook-login').submit();
                        $('#vk-login').submit();
                        return false;
                    } else {
                        self.selected_product(self.get_product(this.params['product_id']))
                        if (self.selected_product()) {
                            self.user().start_send(self.get_product(this.params['product_id']));
                        } else {
                            context.redirect('#/');
                        }
                    }
                });

                this.get('#/product/details/:product_id', function (context) {
                    self.selected_product(self.get_product(this.params['product_id']))
                    if (self.selected_product()) {
                        self.selected_product().show()
                    } else {
                        context.redirect('#/')
                    }
                });

                this.get('#/gift/accept/:gift_id', function (context) {
                    var gift = self.get_gift(this.params['gift_id']);
                    if (gift) {
                        self.user().selected_friend(self.user().get_friend(gift.friend.fb_id));
                        self.selected_gift(gift);
                        console.log('open accept wizard');
                        gift.process_accept();
                    } else {
                        context.redirect('#/my-gifts/');
                    }
                });

                this.get('#_=_', function (context) {
                    if ($.cookie('redirect'))
                        this.redirect($.cookie('redirect'))
                    else
                        this.redirect('#/')
                });


            }).run('#/');
        })

        $.ajaxSetup({cache: false})

    }

    $(document).ajaxStart(function () {
        $('.loader').addClass('loading');
    }).ajaxStop(function () {
        $('.loader').removeClass('loading');
    });

    $.getScript('//vk.com/js/api/openapi.js', function () {
        VK.init({
            apiId: APP_ID
        });
        var product_view_model = new ProductViewModel();
        ko.applyBindings(product_view_model);
    });

})
;
